import React, { Component } from 'react';
// import firebase from 'firebase';
import {
  Col, Button,
} from 'react-bootstrap';

import { Auth } from './../../auth.core';
import { NavHeader } from './../NavHeader/NavHeader';
export class AuthForm extends Component{
  constructor(props, context){
    super(props, context);
    this.state = {
      currentUser: null,
      email: null,
      password: null
    }

    this.signIn = this.signIn.bind(this);
    // this.register = this.register.bind(this);
    // this.signInWithGoogle = this.signInWithGoogle.bind(this);
    this.signOut = this.signOut.bind(this);
    this.logoutButton = this.logoutButton.bind(this);
  };//constructor

  componentDidMount(){

  };//componentDidMount

  logoutButton(){
    if( window.user )
      return (<Button className={'btn btn-xs btn-danger'} onClick={this.signOut}>Sair</Button>);
  }

  signOut(e){
    e.preventDefault();
    Auth.signOut();
  };//signOut

  signIn(e){
    e.preventDefault();
    let email = this.refs.email.value;
    let password = this.refs.password.value;
    Auth.signIn(email,password);

    // if(window.user){
    //   console.log('auth',window.user)
    //   if(window.user.uid){
    //     window.location.reload();
    //   }
    // }
  };//signIn

  render(){
    return (
      <div>
      { this.props.currentUser ?
          <NavHeader />
        :
        <form name={'login'} className={'text-left'} style={{marginTop:22}}>
          <Col md={3} className={'col-md-offset-2'}>
            <div className="input-group">
              <span className="input-group-addon" id="basic-addon1">E-mail</span>
              <input
                className={'form-control'}
                type="email"
                ref="email"
                placeholder="" />
            </div>
          </Col>
          <Col md={3} className={'no-padding'}>
            <div className="input-group">
              <span className="input-group-addon" id="basic-addon1">Password</span>
              <input
                className={'form-control'}
                type="password"
                ref="password" />
            </div>
          </Col>
          <Col md={1} className={'no-padding'}>
            <Button className={'btn btn-md btn-success'} onClick={this.signIn}>Entrar</Button>
          </Col>
          <Col md={3} style={{marginTop:7}} className={'text-center no-padding'}>
            <Button className={'btn btn-xs btn-primary'} onClick={this.register}>Registrar-se</Button>
            <Button className={'btn btn-xs btn-primary'} onClick={this.signInWithGoogle}>Entrar com google</Button>
            {this.logoutButton()}
          </Col>
        </form>
      }
      </div>
    );
  }
}
